#ifndef MAGLEV_PLUGIN_H
#define MAGLEV_PLUGIN_H


// Gazebo includes
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
// #include <gazebo/common/common.hh>
// #include <gazebo/transport/transport.hh>
// #include <gazebo/msgs/msgs.hh>

// ROS includes
#include <thread>
#include "ros/ros.h"
#include "ros/callback_queue.h"
// #include "ros/subscribe_options.h"

// Boost includes
#include <boost/thread/mutex.hpp>

// Local includes
#include "gzmagnetic/Current.h"


namespace gazebo {
    
    /// \brief Plugin for magnetic computations.
    class GzMagneticPlugin : public ModelPlugin {
        
    public:
        
        /// \brief Constructor.
        GzMagneticPlugin();
        
        /// \brief Destructor.
        virtual ~GzMagneticPlugin();
        
        /// \brief Loads the plugin.
        virtual void Load(physics::ModelPtr model, sdf::ElementPtr sdf);
        
        /// \brief Called by the world update start event.
        void OnUpdate(const common::UpdateInfo& info);
        
        /// \brief Set the current supplied in the selected coil.
        void SetCurrent(const std::string& coil_name, const double& current);
        
        /// \brief Handle an incoming message from ROS.
        void OnRosMsg(const gzmagnetic::CurrentConstPtr& msg);
        
        /// \brief ROS helper function that processes messages
        void QueueThread();
        
        /// Structure describing a magnet
        struct Magnet {
            std::string name;
            math::Vector3 magnetisation;
        };
        
        /// Structure describing a coil
        struct Coil {
            std::string name;
            double radius;
            unsigned int nbLoops;
            double suppliedCurrent;
        };
        
        
    private:
        
        /// \brief Compute the magnetic field B created by a magnetic dipole using the magnetic dipole approximation.
        /// \param p_self Pose of the dipole in world
        /// \param point Point where to compute the magnetic field in world
        /// \param m Magnetic moment of the dipole (in its own reference frame)
        math::Vector3 ComputeMagneticField(const math::Pose& p_self, const math::Vector3& m_self, const math::Pose& point);
        
        /// \brief Compute the force and the torque between 2 dipoles (effort of d1 over d2) using the magnetic dipole approximation.
        /// \param[in] p1 Pose of the first dipole (considered as the magnetic field source)
        /// \param[in] m1 Magnetic moment of the first dipole
        /// \param[in] p2 Pose of the second dipole (on which the efforts are computed)
        /// \param[in] m2 Magnetic moment of the second dipole
        /// \param[out] force Computed magnetic force
        /// \param[out] torque Computed magnetic torque
        void ComputeForceTorque(const math::Pose& p1, const math::Vector3& m1, const math::Pose& p2, const math::Vector3& m2, math::Vector3& force, math::Vector3& torque);
        
        
    private:
        
        static const double mu_0; ///< Magnetic permeability of free space 4 * pi * 10^-7 T.m/A.
        
        std::vector<Magnet> magnets; ///< List of magnets.
        
        std::vector<Coil> coils; ///< List of coils.
        
        physics::ModelPtr model; ///< Pointer to the model.
        
        event::ConnectionPtr update_connection; ///< Pointer to the update event connection.
        
        boost::mutex lock_coils; ///< Semaphore for accessing magnets and coils variables.
        
        /// Message passing interface
        
//         transport::NodePtr node; ///< A node used for transport.
        
//         transport::SubscriberPtr sub; ///< A subscriber to a named topic.
        
        /// ROS interface
        
        std::unique_ptr<ros::NodeHandle> rosNode; ///< A node used for ROS transport.
        
        ros::Subscriber rosSub; ///< A ROS subscriber.
        
        ros::CallbackQueue rosQueue; ///< A ROS callbackqueue that helps process messages.
        
        std::thread rosQueueThread; ///< A thread the keeps running the rosQueue.
    };
}

#endif // MAGLEV_PLUGIN_H
