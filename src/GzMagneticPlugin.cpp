// Local includes
#include "GzMagneticPlugin.h"

// ROS includes
#include "ros/subscribe_options.h"


namespace gazebo {
    
    const double GzMagneticPlugin::mu_0 = 4 * M_PI * 1e-7;
    
    
    GzMagneticPlugin::GzMagneticPlugin() : ModelPlugin() {
        
    }
    
    
    GzMagneticPlugin::~GzMagneticPlugin() {
        event::Events::DisconnectWorldUpdateBegin(update_connection);
        rosQueue.clear();
        rosQueue.disable();
        rosNode->shutdown();
        rosQueueThread.join();
        rosNode.reset();
    }
    
    
    void GzMagneticPlugin::Load(physics::ModelPtr model, sdf::ElementPtr sdf) {
        // Safety check
        if (model->GetJointCount() == 0) {
            gzerr << "Invalid joint count, GzMagneticPlugin not loaded.\n";
            return;
        }
        
        // Store the model pointer for convenience.
        this->model = model;
        
        // Default nb of magnetic elements
        int nbElements = 0;
        // Check that the nbElements element exists, then read the value
        if (sdf->HasElement("nbElements"))
            nbElements = sdf->Get<int>("nbElements");
//         std::cerr << "Nb of magnetic elements: " << nbElements << std::endl;
        
        sdf::ElementPtr element;
        for (unsigned int i = 0; i < nbElements; i++) {
            std::string referenceNo = std::string("reference") + std::to_string(i);
            element = sdf->GetElement(referenceNo);
            
            std::string name;
            if (element->HasElement("name"))
                name = element->Get<std::string>("name");
            
            std::string type;
            if (element->HasElement("type"))
                type = element->Get<std::string>("type");
            
            if (type.compare("coil") == 0) {
                double radius = 0.1;
                if (element->HasElement("radius"))
                    radius = element->Get<double>("radius");
                
                int nbLoops = 1;
                if (element->HasElement("nbLoops"))
                    nbLoops = element->Get<int>("nbLoops");
                
                double suppliedCurrent = 0.;
                if (element->HasElement("suppliedCurrent"))
                    suppliedCurrent = element->Get<double>("suppliedCurrent");
                
                Coil C;
                C.name = name;
                C.radius = radius;
                C.nbLoops = nbLoops;
                C.suppliedCurrent = suppliedCurrent;
                
                coils.push_back(C);
            }
            else if (type.compare("magnet") == 0) {
                math::Vector3 magnetisation(0., 0., 0.);
                if (element->HasElement("magnetisation")) {
                    sdf::ElementPtr mag = element->GetElement("magnetisation");
                    if (mag->HasElement("x"))
                        magnetisation.x = mag->Get<double>("x");
                    if (mag->HasElement("y"))
                        magnetisation.y = mag->Get<double>("y");
                    if (mag->HasElement("z"))
                        magnetisation.z = mag->Get<double>("z");
                }
                
                Magnet M;
                M.name = name;
                M.magnetisation = magnetisation;
                
                magnets.push_back(M);
            }
            else {
                std::cerr << "Type " << type << " was not recognized." << std::endl;
            }
        }
        
        for (auto m : magnets) {
            gzdbg << "magnet name: " << m.name << " magnetisation: " << m.magnetisation << std::endl;
        }
        for (auto c : coils) {
            gzdbg << "coil name: " << c.name << " radius: " << c.radius << " nb loops: " << c.nbLoops << " current: " << c.suppliedCurrent << std::endl;
        }

        // Listen to the update event.
        // This event is broadcast every simulation iteration.
        update_connection = event::Events::ConnectWorldUpdateBegin(boost::bind(&GzMagneticPlugin::OnUpdate, this, _1));
        
//         // Create the node for message passing interface
//         node = transport::NodePtr(new transport::Node());
//         #if GAZEBO_MAJOR_VERSION < 8
//             node->Init(this->model->GetWorld()->GetName());
//         #else
//             node->Init(this->model->GetWorld()->Name());
//         #endif
//         
//         // Create a topic name
//         std::string topicName = "~/" + this->model->GetName() + "/current_cmd";
//         
//         // Subscribe to the topic, and register a callback
//         sub = node->Subscribe(topicName, &GzMagneticPlugin::OnMsg, this);
        
        // Initialize ros, if it has not already been initialized.
        if (!ros::isInitialized()) {
            int argc = 0;
            char **argv = NULL;
            ros::init(argc, argv, "gazebo_client", ros::init_options::NoSigintHandler);
        }
        
        // Create our ROS node.
        rosNode.reset(new ros::NodeHandle("gazebo_client"));
        rosNode->setCallbackQueue(&rosQueue);
        
        // Create a named topic, and subscribe to it.
        ros::SubscribeOptions so = ros::SubscribeOptions::create<gzmagnetic::Current>("/" + this->model->GetName() + "/current_cmd", 10,
            boost::bind(&GzMagneticPlugin::OnRosMsg, this, _1), ros::VoidPtr(), &this->rosQueue);
        rosSub = rosNode->subscribe(so);
        
        // Spin up the queue helper thread.
        rosQueueThread = std::thread(std::bind(&GzMagneticPlugin::QueueThread, this));
        
        gzmsg << "\nGzMagneticPlugin loaded and attached to model[" << model->GetName() << "]\n";
    }
    
    
    void GzMagneticPlugin::OnUpdate(const common::UpdateInfo & info) {
        lock_coils.lock();
        
        // Calculate the efforts from all the coils on the magnets
        for (auto magnet : magnets) {
            // Get the magnet pose
            physics::LinkPtr magnet_link = model->GetLink(magnet.name);
            math::Pose magnet_pose = magnet_link->GetWorldPose();
            
            // Get the magnetisation in world
            math::Vector3 world_mag = magnet_pose.rot.RotateVector(magnet.magnetisation);
            
            for (auto coil : coils) {
                math::Vector3 force(0., 0., 0.);
                math::Vector3 torque(0., 0., 0.);
                
                // Get the coil pose
                physics::LinkPtr coil_link = model->GetLink(coil.name);
                math::Pose coil_pose = coil_link->GetWorldPose();
                
                // Get the coil magnetisation in world
                math::Vector3 z_unit(0., 0., 1.);
                math::Vector3 coil_mag = coil.nbLoops * coil.suppliedCurrent * M_PI * coil.radius * coil.radius * z_unit;
                coil_mag = coil_pose.rot.RotateVector(coil_mag);
                
                // Compute the magnetic force and torque
                ComputeForceTorque(coil_pose, coil_mag, magnet_pose, world_mag, force, torque);
//                 gzdbg << coil.name << " force: " << force << " torque: " << torque << std::endl;
                
                // Apply force and torque
                magnet_link->AddForce(force);
                magnet_link->AddTorque(torque);
            }
        }
        lock_coils.unlock();
    }
    
    
    void GzMagneticPlugin::SetCurrent(const std::string& coil_name, const double& current) {
        lock_coils.lock();
        for (unsigned int i = 0; i < coils.size(); ++i) {
            if (coils[i].name.compare(coil_name) == 0)
                coils[i].suppliedCurrent = current;
        }
        lock_coils.unlock();
    }
    
    
    void GzMagneticPlugin::OnRosMsg(const gzmagnetic::CurrentConstPtr& msg) {
        std::vector<std::string> names = msg->name;
        std::vector<double> currents = msg->current;
        if (names.size() != currents.size()) {
            gzerr << "gzmagnetic::Current message corrupted" << std::endl;
            return;
        }
        
        for (unsigned int i = 0; i < names.size(); ++i) {
            SetCurrent(names[i], currents[i]);
        }
    }
    
    
    math::Vector3 GzMagneticPlugin::ComputeMagneticField(const math::Pose& p_self, const math::Vector3& m_self, const math::Pose& point) {
        math::Vector3 B(0., 0., 0.);
        math::Vector3 r = point.pos - p_self.pos;
        math::Vector3 r_unit = r / r.GetLength();
        double K = mu_0 / (4 * M_PI * pow(r.GetLength(), 3));
        B = K * (3 * r_unit * m_self.Dot(r_unit) - m_self);
        return B;
    }
    
    
    void GzMagneticPlugin::ComputeForceTorque(const math::Pose& p1, const math::Vector3& m1, const math::Pose& p2,
                                          const math::Vector3& m2, math::Vector3& force, math::Vector3& torque) {
        math::Vector3 r = p2.pos - p1.pos;
        math::Vector3 r_unit = r/r.GetLength();
        
        double K = 3. * mu_0 / (4 * M_PI * pow(r.GetLength(), 4));
        force = K * (m2 * (m1.Dot(r_unit)) +  m1 * (m2.Dot(r_unit)) + r_unit * (m1.Dot(m2)) - 5 * r_unit * (m1.Dot(r_unit)) * (m2.Dot(r_unit)));
        
        math::Vector3 B1 = ComputeMagneticField(p1, m1, p2);
        torque = m2.Cross(B1);
    }
    
    
    void GzMagneticPlugin::QueueThread() {
        static const double timeout = 0.01;
        while (rosNode->ok()) {
            rosQueue.callAvailable(ros::WallDuration(timeout));
        }
    }
    
    
    GZ_REGISTER_MODEL_PLUGIN(GzMagneticPlugin)
}
