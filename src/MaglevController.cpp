// ROS includes
#include "ros/ros.h"

// Local includes
#include "gzmagnetic/Current.h"


int main(int argc, char **argv) {
    if (!ros::isInitialized()) {
        ros::init(argc, argv, "maglev_controller");
    }
    
    ros::NodeHandle n;
    
    ros::Publisher current_pub = n.advertise<gzmagnetic::Current>("/maglev/current_cmd", 10);
    
    double freq = 100.;
    ros::Rate loop_rate(freq);
    
    
    int count = 0;
    std::vector<std::string> names;
    names.push_back("bottom_coil");
    names.push_back("top_coil");
    gzmagnetic::Current msg;
    msg.name = names;
    while (ros::ok()) {
        std::vector<double> currents;
        currents.push_back(0.);
        double T = 10.;
        double i;
        i = (double)count;
        i = -5.0 - 5.0 * sin(i * 2 * M_PI / (T * freq));
        std::cout << i << std::endl;
        currents.push_back(i);
        msg.current = currents;
        current_pub.publish(msg);
        ros::spinOnce();
        ++count;
        loop_rate.sleep();
    }
    
    return 0;
}
