This is a Gazebo model plugin that uses the magnetic dipole approximation to compute the efforts between coils and magnets.

## Building the plugin

The plugin is a ros package so the build process is the same as any other package.

```bash
$ cd ~/catkin_ws/src
$ git clone https://framagit.org/Basti/gazebo-magnetic-plugin.git
$ catkin_make -C ~/catkin_ws
```

## Running Example

To run the example in the world/ directory run

```
$ roslaunch gzmagnetic maglev.world
```

The simulation is paused by default and initialized with a current of 10 A in the top coil.

To launch the controller of run

```
$ rosrun gzmagnetic maglev_controller
```
